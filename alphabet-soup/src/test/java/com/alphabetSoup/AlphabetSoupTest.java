package com.alphabetSoup;

import jdk.jshell.execution.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


// Attemted to write Unit tests for the methods. However, it is not a good practice to write test cases for private methods.
// Web resources provided a pattern to to use reflection to test private methods. However, this requires more time and research.
// My intent was to use mock values to drive declared methods and unit test the logic for each, but i ran out of time to redo the code in a separate class rather than the main.

public class AlphabetSoupTest {

    @Mock
    static String testInputFilePath = "testPath";
    @Mock
    static String testInputFilename = "TestFilename";

    @Test
    void contextLoads() {
        assertTrue(true);
    }



//    @SneakyThrows
    private Method openInputFileMethod()  {
        Method method;
        try {
            method = Util.class.getDeclaredMethod("openInputFile", java.lang.String.class, java.lang.String.class);
        } catch (Exception e) {
            System.out.println(e.getMessage().toString());
            throw new RuntimeException(e);
        }
//        method.setAccessible(true);
        return method;
    }

//    @SneakyThrows
    @Test
    void openInputFileTest() throws NoSuchMethodException {
        //            assertTrue((Boolean) openInputFileMethod()  //.invoke("TestPath", "TestFileName"));
//        String name = openInputFileMethod().getName().toString();
        Method method = openInputFileMethod();

        System.out.println("methods: "+method);

    }




}
