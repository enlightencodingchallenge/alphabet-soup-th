package com.alphabetSoup;

import lombok.SneakyThrows;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static java.lang.System.exit;
//import lombok.sneakyThrows;

//Solution strategy: determine loops to traverse the whole grid, left to right, top to bottom, diagonally and backwards in every direction.
//This leads to writing 7 methods, one for each direction.

public class AlphabetSoup {

    //Declaring Global Fields

    static String inputFilePath = "c:\\applicationTest\\input\\";
    static String inputFilename = "croswordFile.txt";
    static String outputFilePath = "c:\\applicationTest\\output\\";
    static String outputFilename = "croswordFileResultsBy_TH.txt";
    static String[] linesinFile;
    static List<String> wordList;
    static List<String> grid;
    static int rowSize;
    static int columnSize;
    static int numberOfLines;
    static List<String> result = new ArrayList<>();
    static HashMap<String, String> diagonalElementsWithLocation = new HashMap<>();


//    @SneakyThrows
    public static void main(String[] args) throws IOException {
        //Read input file
        Boolean validInputFile = openInputFile(inputFilePath, inputFilename);

        if(validInputFile) {

            //Looping strategies for nxn grid or word puzzle
            readAcrossLeftToRight(wordList, grid);
            readAcrossRightToLeft(wordList, grid);
            readAcrossTopToBottom(wordList, grid);
            readAcrossBottomToTop(wordList, grid);
            readAcrossDiagonalTopToBottomLeftToRight(wordList, grid);
            readAcrossDiagonalTopToBottomRightToLeft(wordList, grid);
            ReverseSearchforAllDiagonalValues(wordList, grid);

        } else {
            exit(0);
        }
        //Capture results in a text file and write it to the specified path
        generateOutputFile();

        System.out.println("Result Set: \n"+result);

    }

    private static void generateOutputFile() throws IOException {

        FileWriter writer = new FileWriter(outputFilePath+outputFilename);
        for(String result: result) {
            writer.write(result + System.lineSeparator());
        }
        writer.close();

    }

    private static void ReverseSearchforAllDiagonalValues(List<String> wordList, List<String> grid) {

        HashMap<String, String> tempDiagonalElementsWithLocation = new HashMap<>();

        for (HashMap.Entry<String, String> set: diagonalElementsWithLocation.entrySet() ) {
            String keyValueReverssed = new StringBuilder(set.getKey()).reverse().toString();
            tempDiagonalElementsWithLocation.put(keyValueReverssed,set.getValue());
        }

        diagonalElementsWithLocation.putAll(tempDiagonalElementsWithLocation);

        searchForWordsInTheGridInDiagonalIterationsOnly();





        System.out.println("Diagonal values: "+ diagonalElementsWithLocation);
    }

    private static void searchForWordsInTheGridInDiagonalIterationsOnly() {

        for (HashMap.Entry<String, String> set: diagonalElementsWithLocation.entrySet() ) {
            for (String word: wordList ) {
                if(set.getKey().contains(word)) {
                    String[] firstAndLastLocation = set.getValue().split(" ");
                    System.out.println("locations: "+firstAndLastLocation);
                    result.add(word+" "+firstAndLastLocation[0] + " "+firstAndLastLocation[firstAndLastLocation.length-1]);
                }
            }
        }

    }

    private static void readAcrossDiagonalTopToBottomRightToLeft(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);
        List<String>  gridWithNoSpaces3 = new ArrayList<>(gridWithNoSpaces);
        StringBuilder tempDiagonalValue = new StringBuilder();
        List<String> diagonalValues = new ArrayList<>();
        String elementLocations = new String();
        int index = rowSize-1;

        for (int iterator=0; iterator<rowSize; iterator++) {
            int rowCounter=iterator;
            for (String row: gridWithNoSpaces) {
                tempDiagonalValue.append(row.charAt(index));
                elementLocations = elementLocations.concat(rowCounter+":"+index+" ");
                rowCounter++;
                index--;
            }
            elementLocations = elementLocations.substring(0,elementLocations.length()-1);
            diagonalValues.add(String.valueOf(tempDiagonalValue));
            diagonalElementsWithLocation.put(String.valueOf(tempDiagonalValue), elementLocations);
            elementLocations="";
            gridWithNoSpaces.remove(0);
            tempDiagonalValue.delete(0,tempDiagonalValue.length());
            index=rowSize-1;
        }
        diagonalElementsWithLocation.remove(diagonalValues.getLast());
        diagonalValues.remove(diagonalValues.removeLast());

        index=columnSize-2;

        for (int columnCount=0; columnCount<columnSize-1 & index>=0; columnCount++) {
            int rowCounter=0;
            for (String row: gridWithNoSpaces3) {
                if(index<columnSize & index >= 0) {
                    tempDiagonalValue.append(row.charAt(index));
                    elementLocations = elementLocations.concat(rowCounter+":"+index+" ");
                    rowCounter++;
                    index--;
                }
            }
            elementLocations = elementLocations.substring(0,elementLocations.length()-1);
            diagonalValues.add(String.valueOf(tempDiagonalValue));
            diagonalElementsWithLocation.put(String.valueOf(tempDiagonalValue), elementLocations);
            elementLocations="";
            if(!gridWithNoSpaces3.isEmpty()) { gridWithNoSpaces3.remove(0); }
            tempDiagonalValue.delete(0,tempDiagonalValue.length());
            index=columnSize-2;
        }
        diagonalElementsWithLocation.remove(diagonalValues.getLast());
        diagonalValues.remove(diagonalValues.removeLast());

        System.out.println("diagonal elements: "+ diagonalValues);

        searchForWordsInTheGridInDiagonalIterationsOnly();

    }

    private static void readAcrossDiagonalTopToBottomLeftToRight(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);
        List<String>  gridWithNoSpaces2 = new ArrayList<>(gridWithNoSpaces);
        StringBuilder tempDiagonalValue = new StringBuilder();
        List<String> diagonalValues = new ArrayList<>();
        String elementLocations = new String();
        int index = 0;

        for (int iterator=0; iterator<rowSize; iterator++) {
            int rowCounter=iterator;
            for (String row: gridWithNoSpaces) {
                tempDiagonalValue.append(row.charAt(index));
                elementLocations = elementLocations.concat(rowCounter+":"+index+" ");
                rowCounter++;
                index++;
            }
            elementLocations = elementLocations.substring(0,elementLocations.length()-1);
            System.out.println("element locations: "+elementLocations);
            diagonalValues.add(String.valueOf(tempDiagonalValue));
            diagonalElementsWithLocation.put(String.valueOf(tempDiagonalValue), elementLocations);
            elementLocations="";
            gridWithNoSpaces.remove(0);
            tempDiagonalValue.delete(0,tempDiagonalValue.length());
            index=0;
        }
        diagonalElementsWithLocation.remove(diagonalValues.getLast());
        diagonalValues.remove(diagonalValues.removeLast());

        index=1;

        for (int iterator=1; iterator<rowSize; iterator++) {
            int rowCounter=0;
            for (String row: gridWithNoSpaces2) {
                if(index<columnSize) {
                    tempDiagonalValue.append(row.charAt(index));
                    elementLocations = elementLocations.concat(rowCounter+":"+index+" ");
                    rowCounter++;
                    index++;
                }
            }
            elementLocations = elementLocations.substring(0,elementLocations.length()-1);
            diagonalValues.add(String.valueOf(tempDiagonalValue));
            diagonalElementsWithLocation.put(String.valueOf(tempDiagonalValue), elementLocations);
            elementLocations="";
            gridWithNoSpaces2.remove(0);
            tempDiagonalValue.delete(0,tempDiagonalValue.length());
            index=1;
        }
        diagonalElementsWithLocation.remove(diagonalValues.getLast());
        diagonalValues.remove(diagonalValues.removeLast());

        System.out.println("diagonal elements: "+ diagonalValues);

        searchForWordsInTheGridInDiagonalIterationsOnly();

    }

    private static void readAcrossBottomToTop(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);
        List<String> columnList = new ArrayList<>();

        StringBuilder tempColumnvalues = new StringBuilder();

        for(int columnIndex=0; columnIndex<columnSize; columnIndex++){
            for (int rowIndex=0; rowIndex<rowSize; rowIndex++){
                System.out.println("RowValue: "+gridWithNoSpaces.get(rowIndex));
                tempColumnvalues.append(gridWithNoSpaces.get(rowIndex).charAt(columnIndex));
            }
            tempColumnvalues.reverse().toString();
            columnList.add(tempColumnvalues.toString());
            System.out.println("Column List: "+columnList);
            tempColumnvalues.delete(0,tempColumnvalues.length());
            System.out.println("Temp Column Vals: "+tempColumnvalues);
        }

        for(String word: wordList){
            for(String columnValue: columnList){
                if(columnValue.contains(word)){
                    int startingRowIndex = columnValue.indexOf(columnValue.charAt(columnValue.length()-1)) ;
                    System.out.println("Starting Row Index: "+ startingRowIndex);
                    int startingColumnIndex = columnList.indexOf(word);
                    System.out.println("Starting Column Index: "+ startingColumnIndex);

                    int endingRowIndex = columnValue.indexOf(word.charAt(0));
                    System.out.println("Ending Row Index: "+ endingRowIndex);
                    int endingColumnIndex = columnList.indexOf(word);
                    System.out.println("Starting Column Index: "+ endingColumnIndex);
                    result.add(word +" "+ startingRowIndex + ":" + startingColumnIndex + " " + endingRowIndex + ":" + endingColumnIndex );
                }
            }
        }

    }

    private static void readAcrossTopToBottom(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);
        List<String> columnList = new ArrayList<>();
        int index=0;
        StringBuilder tempColumnvalues = new StringBuilder();

        for(int columnIndex=0; columnIndex<columnSize; columnIndex++){
            for (int rowIndex=0; rowIndex<rowSize; rowIndex++){
                System.out.println("RowValue: "+gridWithNoSpaces.get(rowIndex));
                tempColumnvalues.append(gridWithNoSpaces.get(rowIndex).charAt(columnIndex));
            }
            columnList.add(tempColumnvalues.toString());
            System.out.println("Column List: "+columnList);
            tempColumnvalues.delete(0,tempColumnvalues.length());
            System.out.println("Temp Column Vals: "+tempColumnvalues);
        }

        for(String word: wordList){
            for(String columnValue: columnList){
                if(columnValue.contains(word)){
                    int startingRowIndex = columnValue.indexOf(word.charAt(0)) ;
                    System.out.println("Starting Row Index: "+ startingRowIndex);
                    int startingColumnIndex = columnList.indexOf(word);
                    System.out.println("Starting Column Index: "+ startingColumnIndex);

                    int endingRowIndex = columnValue.indexOf(columnValue.charAt(columnValue.length()-1));
                    System.out.println("Ending Row Index: "+ endingRowIndex);
                    int endingColumnIndex = columnList.indexOf(word);
                    System.out.println("Starting Column Index: "+ endingColumnIndex);
                    result.add(word +" "+ startingRowIndex + ":" + startingColumnIndex + " " + endingRowIndex + ":" + endingColumnIndex );
                }
            }
        }
    }

    private static void readAcrossRightToLeft(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);

        for (String word: wordList){
            for (String row: gridWithNoSpaces){
                String reverseRowValues = new StringBuffer(row).reverse().toString();
                if(reverseRowValues.contains(word)) {
                    int startingRowIndex = gridWithNoSpaces.indexOf(row) ;    //row.indexOf(word.charAt(0))
                    System.out.println("Starting Row Index: "+ startingRowIndex);
                    int startingColumnIndex = row.indexOf(word.charAt(0)) ;   //  word.indexOf(word.charAt(word.length()-1))
                    System.out.println("Starting Column Index: "+ startingColumnIndex);

                    int endingRowIndex = gridWithNoSpaces.indexOf(row) ;    //row.indexOf(word.charAt(0))
                    System.out.println("Ending Row Index: "+ endingRowIndex);
                    int endingColumnIndex = row.indexOf(word.charAt(word.length()-1)) ;   //  word.indexOf(word.charAt(word.length()-1))
                    System.out.println("Ending Column Index: "+ endingColumnIndex);

                    result.add(word +" "+ startingRowIndex + ":" + startingColumnIndex + " " + endingRowIndex + ":" + endingColumnIndex );
                    System.out.println("Match found for: "+word+" in row: "+row);
                }
            }
        }




    }

    private static void readAcrossLeftToRight(List<String> wordList, List<String> grid) {
        List<String> gridWithNoSpaces = removeSpacesInTheGrid(grid);

        System.out.println("Removed all spaces in the TEMP GRID: ");
        gridWithNoSpaces.stream().forEach(System.out::println);

        for (String word: wordList){
            for (String row: gridWithNoSpaces){
                if(row.contains(word)) {
                    int startingRowIndex = gridWithNoSpaces.indexOf(row) ;    //row.indexOf(word.charAt(0))
                    System.out.println("Starting Row Index: "+ startingRowIndex);
                    int startingColumnIndex = row.indexOf(word.charAt(0)) ;   //  word.indexOf(word.charAt(word.length()-1))
                    System.out.println("Starting Column Index: "+ startingColumnIndex);

                    int endingRowIndex = gridWithNoSpaces.indexOf(row) ;    //row.indexOf(word.charAt(0))
                    System.out.println("Ending Row Index: "+ endingRowIndex);
                    int endingColumnIndex = row.indexOf(word.charAt(word.length()-1)) ;   //  word.indexOf(word.charAt(word.length()-1))
                    System.out.println("Ending Column Index: "+ endingColumnIndex);

                    result.add(word +" "+ startingRowIndex + ":" + startingColumnIndex + " " + endingRowIndex + ":" + endingColumnIndex );
                    System.out.println("Match found for: "+word+" in row: "+row);
                }
            }
        }
    }

    private static List<String> removeSpacesInTheGrid(List<String> grid) {
        List<String> gridWithNoSpaces = new ArrayList<>();
        String tempRow = new String();

        for(String eachRow: grid){
            tempRow = eachRow.replace(" ","");
            gridWithNoSpaces.add(tempRow);
        }

        return gridWithNoSpaces;

    }

    private static Boolean openInputFile(String filePath, String inputFilename) throws IOException {
        File inputFile = new File(filePath+inputFilename);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String filecontent;
            StringBuilder stringBuilder = new StringBuilder();

            while((filecontent = reader.readLine()) != null){

                stringBuilder.append(filecontent).append("\n");

            }

            evaluateAllGlobalVariables(stringBuilder);

            System.out.println("File Content: \n"+stringBuilder);
            System.out.println("File Content Size: "+stringBuilder.length());

            return true;
        } catch (FileNotFoundException e) {

            throw new RuntimeException(e);
        }


    }

    private static void evaluateAllGlobalVariables(StringBuilder stringBuilder) {

        System.out.println("Row Size: "+stringBuilder.charAt(0));
        System.out.println("Symbol: "+stringBuilder.charAt(1));
        System.out.println("Column Size: "+stringBuilder.charAt(2));

        String fullContent = String.valueOf(stringBuilder);
        linesinFile = fullContent.split("\n");
        numberOfLines = linesinFile.length;
        System.out.println("Total number of lines: "+numberOfLines);
        String firstLine = linesinFile[0];
        System.out.println("First Line: "+firstLine);
        String[] rowAndColumn = firstLine.split("x");
        rowSize = Integer.valueOf(rowAndColumn[0]);
        columnSize = Integer.valueOf(rowAndColumn[1]);

        System.out.println("Row: "+ rowSize);
        System.out.println("Column: "+ columnSize);

        grid = Arrays.stream(linesinFile).toList().subList(1, rowSize +1);
        System.out.println("Grid Details:");
        grid.stream().forEach(System.out::println);

        wordList = Arrays.stream(linesinFile).toList().subList(rowSize +1, linesinFile.length);
        System.out.println("List of Words: ");
        wordList.stream().forEach(System.out::println);

    }
}